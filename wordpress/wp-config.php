<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{VCdHz)<G=Q23{3B=6..by%jS<=ZMcD4[<.-,<-a!}5VNN5yBI=dG+hj)F5WN?[_');
define('SECURE_AUTH_KEY',  'smS(W~bun$se)37Xd4j*RK2+rj{0|vu0HdPX97,wqK?y>c49_O,T0 )Vl]ctm62R');
define('LOGGED_IN_KEY',    'jV1f^t%Vj*$Au_q664/##fu+?]jv&yt35$~fjTH[p3BIZwBkFe7q0f%il@/nA&_ ');
define('NONCE_KEY',        'Z7B+_MvENb<wF2V|:Z3AD;S&i+f5[RvDP_uP^5iLPbIB.^P[.y!T8-k6v,VtKO!$');
define('AUTH_SALT',        'FB|#yI:;*kV8,0]7|`$pJlZ$*R*Y[V^N.alFs2RkQ}laIJ~G,|6Z^ #[4_m@~iks');
define('SECURE_AUTH_SALT', '[}oROHL#;(zK$*i- 7<vwAn)-U7#|b<j:Vu<~7PK{v^bs5c_o{TmY}(!+#fIQqkT');
define('LOGGED_IN_SALT',   ':y/e>JZ.jI4/bE*_P!tI(`nZY0O^m{sb.Agm6Z[xQF?J=yspvkuAG23/Gtm7!{@y');
define('NONCE_SALT',       '8{^)^.Tz3 -fx,}~(n sQR4=In)$%~`!VI8-z1{8Nv}A{OM7lWm+iF,)O?#/wca{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
