# Workflow
***
Go with the flow bruur.

## Introduction
***
For working on the development of LemonCake websites, you need the following programs:

1. [MAMP](https://www.mamp.info/en/) (Running the website Locally)
2. [SourceTree](https://www.sourcetreeapp.com/) (Editing the Repos)

and have access to these websites:

3. [BitBucket](https://bitbucket.org/lemonbucket/) (Git like environment)
4. [DeployBot](https://lemoncake.deploybot.com/) (Deployment of repo/branches to the server)

Developers need access to all of the above.
If you only need to push the site to live you only need access to [BitBucket](https://bitbucket.org/lemonbucket/)  and [DeployBot](https://lemoncake.deploybot.com/).

## Walkthrough for main Development
***
To make sure this process will go as smoothly as possible be sure to read the manual carefully and call out any discrepancies in this guides.

### Step 1
***
Download [MAMP](https://www.mamp.info/en/) and [SourceTree](https://www.sourcetreeapp.com/).

#### Setting up MAMP:

[MAMP](https://www.mamp.info/en/) should be good from the get go. If this is not the case the **Web Server** settings should have **Apache** ticked.
Double check the **Document Root** to make sure you are editing the files in the correct directory of your choosing.

If all else fails be sure to get the "lead developer" on the case. He/She might be able to help you further.

#### Setting up SourceTree:

When you are in the installation process of [SourceTree](https://www.sourcetreeapp.com/) make sure you create or have an [Atlassian](https://www.atlassian.com/) account ready. If you don't have one you should create one with the Google login using your lemoncake credentials.

When you are prompted for a *full name* be sure to fill in your **own name**. Otherwise we can't track your edits/commits/pulls ect. in the repo.

When prompted to connect an account select the `BitBucket` in combination with `OAuth`.
This should prompt you to the website and it will ask you to **grant access**.

Continue the next screen.
On the popup answer `no`.

Skip setup on the next screen.

You are now ready to go... kinda.

#### Setting up an SSH-Key

For security reasons you need to add your pc/laptop to the SSH in the BitBucket account settings.
Follow [this](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html) tutorial on setting up this feat

Windows users will need [this](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).

When you have the SSH-Key add it to the BitBucket SSH-Keys with an apt description.

### Step 2
***
When working on code clone the repo to a folder. If you did **step 1** correctly this should pose no problems.
Be sure the cloned directory is the same as the **Document Root** we had earlier in [MAMP](https://www.mamp.info/en/).
When working on the code be sure to commit once you are done for the day or the files need to pushed to the test/master branch.
If you are done with the code first commit, check all changes add a relevant commit message so we know that the changes entail.
Click commit and the changes are committed **locally**. To push the changes to the BitBucket repo push the changes.

*That's all folks*

If you need to make branches or other changes be sure to Commit & Push them. Otherwise they will only be done locally.

## Walkthrough for Deployment
***

For this you only need access to [DeployBot](https://lemoncake.deploybot.com/) and you need to know the structure of the repo that needs to be deployed.

### Step 1 - Connecting a Repo
***
Log into the [DeployBot](https://lemoncake.deploybot.com/) environment.
When you are on the Dashboard navigate to the repositories.

Here you can find the `Connect a repository` button. You'll need to click this button if you want to add a new repository.

When connecting a new repo you'll need to select the `BitBucket` option and select the correct account (Select the account that has the repo). Afterwards you'll need to select the repo. Give the newly connect a good name so we'll know what it is and you're good to go. Color label is not a necessity, but why not?

### Step 2 - Creating an Environment & Server
***

If you've just hooked up the repo to DeployBot give it a few moments to initialise.
navigate to the repo to setup it's environments.

Click the `Create environment & server` to start setting up the repo for deployment.

#### Step 2.1 - Creating the Environment
***
##### General Settings
In general settings make sure the name reflects the task like `deploy development X-site` or `compile and deploy to test`.
1. For deployment to **DEVEL** use the red color label
2. For deployment to **TEST** use the yellow color label
3. For deployment to **MASTER** use the green color label

##### Deployment mode

For testing and development deployment use the automatic setting. When commits are in and pushed they will be automatically deployed.

For master always use manual. Why? if there are still some minor bugs or things in the build that shouldn't be there it's already to late when the commit has been done. This acts as a failsafe so we are sure when we deploy the master it has has the LemonCake seal of approval. To deploy the repo just click deploy on the website.

##### Branch

Set the branch to the branch [DeployBot](https://lemoncake.deploybot.com/) should be watching.
[DeployBot](https://lemoncake.deploybot.com/) will "listen" to the incoming changes and will do its thing.

##### Triggers

Tick the `Notify Slack`. Set it to the `lemoncake` account and let it post to `gitupdate`.
This will allow [DeployBot](https://lemoncake.deploybot.com/) to notify the group once the deployment is finished. Make sure you are in the [#gitupdate](https://lemoncake.slack.com/messages/C7N7J4GE7) group in slack.

#### Step 2.2 - Adding the Server
***
Choose the FTP option.
Fill in the server data.
If files need to be excluded from the deployment to ftp add them in the textarea that states `Exclude certain paths from being uploaded`.

Check the repo for the files to exclude.

Once done press `save` and you are good to go.

***
# Walkthrough for new project setup
***
1. Create a hosting environment for devel.domain.com, test.domain.com, www.domain.com
2. Create a Bitbucket repository for the project
3. Create a *master*, *test* and *devel* branch
4. Create a repository in DeployBot for the project
5. Create a development environment with auto deploy, link notifications to the Slack [#gitupdate](https://lemoncake.slack.com/messages/C7N7J4GE7) channel and connect to the devel.domain.com FTP.
6. Create a testing environment with auto deploy, link notifications to the Slack [#gitupdate](https://lemoncake.slack.com/messages/C7N7J4GE7) channel and connect to the test.domain.com FTP.
7. Create a live environment with manual deploy, link notifications to the Slack [#gitupdate](https://lemoncake.slack.com/messages/C7N7J4GE7) channel and connect to the www.domain.com FTP.
8. Follow the 'Workflow for existing projects' instructions


***
# Workflow for existing projects
***
## Git branching strategy

### Main branches
1. **master:** reflects to the contents of the live envirement (www.example.com), or a production-ready / go-live-ready state.
2. **test:** reflects to the contents of the test envirement (test.example.com)
3. **devel:** reflects to the contents of the development envirement (devel.example.com)

We consider origin/develop to be the main branch where the source code of HEAD always reflects a state with the latest delivered development changes for the next release.
Some would call this the *integration branch*.
This is where any automatic nightly builds are built from.

When the source code in the *devel* branch reaches a stable point and is ready to be released, all of the changes should be pushed into the *test* branch where final tests will be done and the client can walk through the website in needed.
Minor fixes can be done in this state. When ready to be released the changes should be pushed in to *master*.

When pushing changes in to the *origin/devel* of 'origing/test' branch, DeployBot recognises the changes and is set to automaticly deploy this changes to the FTP of *devel.domain.com* and/or *test.domain.com*.
When pushing changes in tot the *origing/master* branch, you have to manualy deploy the changes to the FTP via DeployBot. It's setup this way to have an extra check-point to have a minimum risc of deploying the wrong data.

### Supporting branches
1. **Feature** branches
2. **Release** branches
3. **Hotfix** branches

Each of these branches have a specific purpose and are bound to strict rules as to which branches may be their originating branch and which branches must be their merge targets.
We will walk through them in a minute.

By no means are these branches *special* from a technical perspective.
The branch types are categorized by how we use them. They are of course plain old Git branches.

### Feature branches
1. May branch off from: **devel**
2. Must merge back into: **devel**
3. Branch naming convention: **feature-***

Feature branches (or sometimes called topic branches) are used to develop new features for the upcoming or a distant future release.
When starting development of a feature, the target release in which this feature will be incorporated may well be unknown at that point.
The essence of a feature branch is that it exists as long as the feature is in development, but will eventually be merged back into *devel* (to definitely add the new feature to the upcoming release) or discarded (in case of a disappointing experiment).

### Release branches
1. Must branch off from: **master**
2. Must merge back into: **devel**, **test** and **master**
3. Branch naming convention: **release-*.*_***

Release branches reflect certain releases that we wish to keep aside as important points / releases in time.
We use it like version management but high over for big development cicles. Like Launch, Added blog, Added webshop, Complete 2.0 re-design etc.

### Hotfix branches
1. May branch off from: **devel**, **test** and **master**
2. Must merge back into: **develop**, **test** and **master**
3. Branch naming convention: **hotfix-***

Hotfix branches are meant to prepare for a new production release, albeit unplanned. They arise from the necessity to act immediately upon an undesired state of a live production version. When a critical bug in a production version must be resolved immediately, a hotfix branch may be branched off from the corresponding tag on the master branch that marks the production version.

The essence is that work of team members (on the *devel* branch) can continue, while another person is preparing a quick production fix.

When finished, the bugfix needs to be merged back into *master*, but also needs to be merged back into *test* and *devel*, in order to safeguard that the bugfix is included in the next release as well.
Finally, remove the temporary branch.
